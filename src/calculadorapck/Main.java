package calculadorapck;
/**
 * @author Miguel
 */
/**
 * Clase principal para mostrar la interfaz de la calculadora
 */
public class Main {
	/**
	 * @param gui clase de la interfaz de la calculadora
	 */
	public static void main (String[] args) {
		CalculadoraGUI gui = new CalculadoraGUI();
		gui.setVisible(true);
	}

}
