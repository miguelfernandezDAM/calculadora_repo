package calculadorapck;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Before;
import org.junit.jupiter.api.Test;
/**
 * @author Miguel
 */
/**
 * Clase de las pruebas de los m�todos de la calculadora
 */
public class CalculaduraTest {
	/**
	 * @param x primer int de la operaci�n
	 * @param y segundo int de la operaci�n
	 * @param c clase de las operaciones de la calculadora
	 */
	int x, y;
	Calculadora c = new Calculadora(x,y);
	/**
	 * Test del m�todo Suma de la clase Calculadora
	 */
	@Test
	void sumaTest() {
		assertEquals(5,c.suma(3, 2),0);
		assertEquals(5,c.suma(2, 3),0);
		assertEquals(0,c.suma(0, 0),0);
		assertNotEquals(4,c.suma(2, 3),0);
	}
	/**
	 * Test del m�todo Resta de la clase Calculadora
	 */
	@Test
	void restaTest() {
		assertEquals(1,c.resta(3, 2),0);
		assertEquals(-1,c.resta(2, 3),0);
		assertEquals(-5,c.resta(0, 5),0);
		assertNotEquals(-1,c.resta(3, 2),0);
		assertEquals(0,c.resta(0, 0),0);
		assertNotEquals(1,c.resta(2, 3),0);
	}
	/**
	 * Test del m�todo Multiplicaci�n de la clase Calculadora
	 */
	@Test
	void multiplicacionTest() {
		assertEquals(6,c.multiplicacion(3, 2),0);
		assertEquals(6,c.multiplicacion(2, 3),0);
		assertNotEquals(6,c.multiplicacion(3, 3));
		assertNotEquals(5,c.multiplicacion(2, 3));
	}
}
