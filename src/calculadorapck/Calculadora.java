package calculadorapck;
/**
 * @author Miguel
 */
/**
 * Clase de las operaciones de la calculadora
 */
public class Calculadora{
	int a, b;
	/**
	 * M�todo constructor de la clase
	 * @param x primer int de la operaci�n
	 * @param y segundo int de la operaci�n
	 */
	public Calculadora(int x, int y) {
		a = x;
		b = y;
	}
	/**
	 * M�todo para sumar dos variables enteras
	 * @param a primer int de la operaci�n
	 * @param b segundo int de la operaci�n
	 * @param r resultado
	 * @return Resultado de a + b
	 */
	public int suma(int a, int b) {
		int r = a + b;
		return r;
	}
	/**
	 * M�todo para restar dos variables enteras
	 * @param a primer int de la operaci�n
	 * @param b segundo int de la operaci�n
	 * @param r resultado
	 * @return Resultado de a - b
	 */
	public int resta(int a, int b) {
		int r = a - b;
		return r;
	}
	/**
	 * M�todo para multiplicar dos variables enteras
	 * @param a primer int de la operaci�n
	 * @param b segundo int de la operaci�n
	 * @param r resultado
	 * @return Resultado de a * b
	 */
	public int multiplicacion(int a, int b) {
		int r = a * b;
		return r;
	}
	/**
	 * M�todo para dividir dos variables enteras
	 * @param a primer int de la operaci�n
	 * @param b segundo int de la operaci�n
	 * @param r resultado
	 * @return Resultado de a / b
	 */
	public int division(int a, int b) {
		int r = a / b;
		return r;
	}
}
