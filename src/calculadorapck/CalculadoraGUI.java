package calculadorapck;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
/**
 * @author Miguel
 */
/**
 * Clase de la interfaz de la calculadora
 * @param a primer int de la calculadora
 * @param b segundo int de la calculadora
 * @param r resultado num�rico de la operacion
 * @param operacion string por el que se define el tipo de operaci�n que se ha de calcular
 */
public class CalculadoraGUI extends JFrame{
	private	int a, b, r;
	private	String operacion ="";
	
	/**
	 * Interfaz de la calculadora
	 * @param c clase de las operaciones de la calculadora
	 * @param resultado �rea de texto donde se muestran los n�meros y resultados de las operaciones 
	 * @param boton1 bot�n que imprime el valor 1 en 'resultado'
	 * @param boton2 bot�n que imprime el valor 2 en 'resultado'
	 * @param boton3 bot�n que imprime el valor 3 en 'resultado'
	 * @param boton4 bot�n que imprime el valor 4 en 'resultado'
	 * @param boton5 bot�n que imprime el valor 5 en 'resultado'
	 * @param boton6 bot�n que imprime el valor 6 en 'resultado'
	 * @param boton7 bot�n que imprime el valor 7 en 'resultado'
	 * @param boton8 bot�n que imprime el valor 8 en 'resultado'
	 * @param boton9 bot�n que imprime el valor 9 en 'resultado'
	 * @param boton0 bot�n que imprime el valor 0 en 'resultado'
	 * @param botonSuma bot�n que adjudica el primer valor impreso como 'a', limpia los datos de 'resultado' y define 'operacion' como "suma"
	 * @param botonResta bot�n que adjudica el primer valor impreso como 'a', limpia los datos de 'resultado' y define 'operacion' como "resta"
	 * @param botonMult bot�n que adjudica el primer valor impreso como 'a', limpia los datos de 'resultado' y define 'operacion' como "multiplicacion"
	 * @param botonDiv bot�n que adjudica el primer valor impreso como 'a', limpia los datos de 'resultado y define 'operacion' como "division"
	 * @param botonLimpiar bot�n que limpia los datos de 'resultado'
	 * @param botonIgual bot�n que adjudica el �ltimo valor impreso como 'b' y calcula y muestra el resultado num�rico en funci�n del valor de 'operacion'

	 */
	public CalculadoraGUI(){
		Calculadora c = new Calculadora(a,b);
		
		//Configuraci�n de la ventana
		this.setTitle("Calculadora");
		this.setSize(460,595);
		this.setLocationRelativeTo(null);		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		this.getContentPane().add(panel);
		panel.setBackground(Color.WHITE);
		
		//�rea de resultados
		JTextField resultado = new JTextField();		
		resultado.setBounds(10,10,432,100);
		resultado.setFont(new Font("arial",Font.BOLD,50));
		panel.add(resultado);
		
		//1
		JButton boton1 = new JButton("1");
		boton1.setBounds(10,120,100,100);
		boton1.setFont(new Font("arial",Font.BOLD,50));
		panel.add(boton1);
		boton1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				String numeroIntroducido = resultado.getText() + boton1.getText();
				resultado.setText(numeroIntroducido);
			}
		});		
		
		//2
		JButton boton2 = new JButton("2");
		boton2.setBounds(120,120,100,100);
		boton2.setFont(new Font("arial",Font.BOLD,50));
		panel.add(boton2);
		boton2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				String numeroIntroducido = resultado.getText() + boton2.getText();
				resultado.setText(numeroIntroducido);
			}
		});
		
		//3
		JButton boton3 = new JButton("3");
		boton3.setBounds(230,120,100,100);
		boton3.setFont(new Font("arial",Font.BOLD,50));
		panel.add(boton3);
		boton3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				String numeroIntroducido = resultado.getText() + boton3.getText();
				resultado.setText(numeroIntroducido);
			}
		});
		
		//4
		JButton boton4 = new JButton("4");
		boton4.setBounds(10,230,100,100);
		boton4.setFont(new Font("arial",Font.BOLD,50));
		panel.add(boton4);
		boton4.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				String numeroIntroducido = resultado.getText() + boton4.getText();
				resultado.setText(numeroIntroducido);
			}
		});
		
		//5
		JButton boton5 = new JButton("5");
		boton5.setBounds(120,230,100,100);
		boton5.setFont(new Font("arial",Font.BOLD,50));
		panel.add(boton5);
		boton5.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				String numeroIntroducido = resultado.getText() + boton5.getText();
				resultado.setText(numeroIntroducido);
			}
		});
		
		//6
		JButton boton6 = new JButton("6");
		boton6.setBounds(230,230,100,100);
		boton6.setFont(new Font("arial",Font.BOLD,50));
		panel.add(boton6);
		boton6.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				String numeroIntroducido = resultado.getText() + boton6.getText();
				resultado.setText(numeroIntroducido);
			}
		});
		
		//7
		JButton boton7 = new JButton("7");
		boton7.setBounds(10,340,100,100);
		boton7.setFont(new Font("arial",Font.BOLD,50));
		panel.add(boton7);
		boton7.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				String numeroIntroducido = resultado.getText() + boton7.getText();
				resultado.setText(numeroIntroducido);
			}
		});
		
		//8
		JButton boton8 = new JButton("8");
		boton8.setBounds(120,340,100,100);
		boton8.setFont(new Font("arial",Font.BOLD,50));
		panel.add(boton8);
		boton8.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				String numeroIntroducido = resultado.getText() + boton8.getText();
				resultado.setText(numeroIntroducido);
			}
		});
		
		//9
		JButton boton9 = new JButton("9");
		boton9.setBounds(230,340,100,100);
		boton9.setFont(new Font("arial",Font.BOLD,50));
		panel.add(boton9);
		boton9.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				String numeroIntroducido = resultado.getText() + boton9.getText();
				resultado.setText(numeroIntroducido);
			}
		});
		
		//0
		JButton boton0 = new JButton("0");
		boton0.setBounds(10,450,100,100);
		boton0.setFont(new Font("arial",Font.BOLD,50));
		panel.add(boton0);
		boton0.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				String numeroIntroducido = resultado.getText() + boton0.getText();
				resultado.setText(numeroIntroducido);
			}
		});
		
		//Limpiar el texto del �rea de resultados
		JButton botonLimpiar = new JButton("C");
		botonLimpiar.setBounds(120,450,100,100);
		botonLimpiar.setFont(new Font("arial",Font.BOLD,50));
		panel.add(botonLimpiar);
		botonLimpiar.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				resultado.setText(null);
			}
		});
		
		//Igual
		JButton botonIgual = new JButton("=");
		botonIgual.setBounds(230,450,100,100);
		botonIgual.setFont(new Font("arial",Font.BOLD,50));
		panel.add(botonIgual);
		botonIgual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {				
				b = Integer.parseInt(resultado.getText());
				switch (operacion) {
					case "suma":
						r = c.suma(a, b);
						break;
					case "resta":
						r  = c.resta(a, b);
						break;
					case "multiplicacion":
						r = c.multiplicacion(a, b);
						break;
					case "division":
						r = (int)c.division(a, b);
						break;
					default:
						break;
				}
				resultado.setText(String.valueOf(r));
				operacion = "";
			}
		});
		
		//Suma
		JButton botonSuma = new JButton("+");
		botonSuma.setBounds(340,120,100,100);
		botonSuma.setFont(new Font("arial",Font.BOLD,50));
		panel.add(botonSuma);
		botonSuma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				a = Integer.parseInt(resultado.getText());
				resultado.setText("");				
				operacion = "suma";
			}
		});
		
		//Resta
		JButton botonResta = new JButton("-");
		botonResta.setBounds(340,230,100,100);
		botonResta.setFont(new Font("arial",Font.BOLD,50));
		panel.add(botonResta);
		botonResta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				a = Integer.parseInt(resultado.getText());
				resultado.setText("");
				operacion = "resta";
			}
		});
		
		//Multiplicaci�n
		JButton botonMult = new JButton("*");
		botonMult.setBounds(340,340,100,100);
		botonMult.setFont(new Font("arial",Font.BOLD,50));
		panel.add(botonMult);
		botonMult.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				a = Integer.parseInt(resultado.getText());
				resultado.setText("");
				operacion = "multiplicacion";
			}
		});
		
		//Divisi�n
		JButton botonDiv = new JButton("/");
		botonDiv.setBounds(340,450,100,100);
		botonDiv.setFont(new Font("arial",Font.BOLD,50));
		panel.add(botonDiv);
		botonDiv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				a = Integer.parseInt(resultado.getText());
				resultado.setText("");
				operacion = "division";
			}
		});
	}
}
